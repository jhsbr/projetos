package br.com.jhs.dlsdata.entitys;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by JHS
 * on 19/11/2015.
 */
public class PessoaHasContatoTelefone extends RealmObject {

    @Ignore
    private final String COMPOSITE_KEY = "%d;%d";

    @PrimaryKey
    private String id;
    private Pessoa pessoa;
    private ContatoTelefone contatoTelefone;

    public PessoaHasContatoTelefone() {}

    public String getId() {
        return id;
    }

    public void setId(String idpessoa, String idcontatoTelefone) {
        id = String.format(COMPOSITE_KEY, idpessoa, idcontatoTelefone);
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public ContatoTelefone getContatoTelefone() {
        return contatoTelefone;
    }

    public void setContatoTelefone(ContatoTelefone contatoTelefone) {
        this.contatoTelefone = contatoTelefone;
    }
}
