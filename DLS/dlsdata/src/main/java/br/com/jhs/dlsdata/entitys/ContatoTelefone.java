package br.com.jhs.dlsdata.entitys;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by JHS
 * on 19/11/2015.
 */
public class ContatoTelefone extends RealmObject {

    @PrimaryKey
    private int idcontatoTelefone;
    private String tipo;
    private String dd;
    private String numero;
    private PessoaHasContatoTelefone pessoaHasContatoTelefone;

    public ContatoTelefone() {}

    public int getIdcontatoTelefone() {
        return idcontatoTelefone;
    }

    public void setIdcontatoTelefone(int idcontatoTelefone) {
        this.idcontatoTelefone = idcontatoTelefone;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDd() {
        return dd;
    }

    public void setDd(String dd) {
        this.dd = dd;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public PessoaHasContatoTelefone getPessoaHasContatoTelefone() {
        return pessoaHasContatoTelefone;
    }

    public void setPessoaHasContatoTelefone(PessoaHasContatoTelefone pessoaHasContatoTelefone) {
        this.pessoaHasContatoTelefone = pessoaHasContatoTelefone;
    }
}
