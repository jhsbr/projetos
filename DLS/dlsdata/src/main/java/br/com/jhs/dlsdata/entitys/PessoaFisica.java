package br.com.jhs.dlsdata.entitys;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by JHS
 * on 19/11/2015.
 */
public class PessoaFisica extends RealmObject {

    @PrimaryKey
    private int idpessoaFisica;
    private String cargo;
    private String cpf;
    private String rg;
    private Pessoa pessoa;

    public PessoaFisica() {}

    public int getIdpessoaFisica() {
        return idpessoaFisica;
    }

    public void setIdpessoaFisica(int idpessoaFisica) {
        this.idpessoaFisica = idpessoaFisica;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }
}
