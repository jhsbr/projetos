package br.com.jhs.dlsdata.entitys;

import java.util.Date;
import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by JHS
 * on 19/11/2015.
 */
public class Pessoa extends RealmObject {

    @PrimaryKey
    private int idpessoa;
    private String endereco;
    private String bairro;
    private String cidade;
    private String cep;
    private Date dta_cadastro;
    private Date dta_alteracao;
    private Login login;
    private List<PessoaHasContatoSocial> pessoaHasContatoSociais;
    private List<PessoaHasContatoTelefone> pessoaHasContatoTelefones;
    private List<ImagemCartaoHasPessoa> imagemCartaoHasPessoas;

    public Pessoa(){}

    public int getIdpessoa() {
        return idpessoa;
    }

    public void setIdpessoa(int idpessoa) {
        this.idpessoa = idpessoa;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public Date getDta_cadastro() {
        return dta_cadastro;
    }

    public void setDta_cadastro(Date dta_cadastro) {
        this.dta_cadastro = dta_cadastro;
    }

    public Date getDta_alteracao() {
        return dta_alteracao;
    }

    public void setDta_alteracao(Date dta_alteracao) {
        this.dta_alteracao = dta_alteracao;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    public List<PessoaHasContatoSocial> getPessoaHasContatoSociais() {
        return pessoaHasContatoSociais;
    }

    public void setPessoaHasContatoSociais(List<PessoaHasContatoSocial> pessoaHasContatoSociais) {
        this.pessoaHasContatoSociais = pessoaHasContatoSociais;
    }

    public List<PessoaHasContatoTelefone> getPessoaHasContatoTelefones() {
        return pessoaHasContatoTelefones;
    }

    public void setPessoaHasContatoTelefones(List<PessoaHasContatoTelefone> pessoaHasContatoTelefones) {
        this.pessoaHasContatoTelefones = pessoaHasContatoTelefones;
    }

    public List<ImagemCartaoHasPessoa> getImagemCartaoHasPessoas() {
        return imagemCartaoHasPessoas;
    }

    public void setImagemCartaoHasPessoas(List<ImagemCartaoHasPessoa> imagemCartaoHasPessoas) {
        this.imagemCartaoHasPessoas = imagemCartaoHasPessoas;
    }
}
