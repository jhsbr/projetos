package br.com.jhs.dlsdata.entitys;

import java.util.Date;
import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by JHS
 * on 19/11/2015.
 */
public class ImagemCartao extends RealmObject {

    @PrimaryKey
    private int idimagem_cartao;
    private byte[] imagem;
    private Date dta_inclusao;
    private List<ImagemCartaoHasPessoa> imagemCartaoHasPessoas;

    public ImagemCartao() {}

    public int getIdimagem_cartao() {
        return idimagem_cartao;
    }

    public void setIdimagem_cartao(int idimagem_cartao) {
        this.idimagem_cartao = idimagem_cartao;
    }

    public byte[] getImagem() {
        return imagem;
    }

    public void setImagem(byte[] imagem) {
        this.imagem = imagem;
    }

    public Date getDta_inclusao() {
        return dta_inclusao;
    }

    public void setDta_inclusao(Date dta_inclusao) {
        this.dta_inclusao = dta_inclusao;
    }

    public List<ImagemCartaoHasPessoa> getImagemCartaoHasPessoas() {
        return imagemCartaoHasPessoas;
    }

    public void setImagemCartaoHasPessoas(List<ImagemCartaoHasPessoa> imagemCartaoHasPessoas) {
        this.imagemCartaoHasPessoas = imagemCartaoHasPessoas;
    }
}
