package br.com.jhs.dlsdata.entitys;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by JHS
 * on 19/11/2015.
 */
public class Login extends RealmObject {

    @PrimaryKey
    private int idlogin;
    private String senha;
    private String email;
    private String id_social;
    private Date dta_ultimo_login;
    private Pessoa pessoa;

    public Login() {}

    public int getIdlogin() {
        return idlogin;
    }

    public void setIdlogin(int idlogin) {
        this.idlogin = idlogin;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId_social() {
        return id_social;
    }

    public void setId_social(String id_social) {
        this.id_social = id_social;
    }

    public Date getDta_ultimo_login() {
        return dta_ultimo_login;
    }

    public void setDta_ultimo_login(Date dta_ultimo_login) {
        this.dta_ultimo_login = dta_ultimo_login;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }
}
