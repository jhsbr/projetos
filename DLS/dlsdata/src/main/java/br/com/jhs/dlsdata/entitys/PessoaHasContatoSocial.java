package br.com.jhs.dlsdata.entitys;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by JHS
 * on 19/11/2015.
 */
public class PessoaHasContatoSocial extends RealmObject {

    @Ignore
    private final String COMPOSITE_KEY = "%d;%d";

    @PrimaryKey
    private String id;
    private Pessoa pessoa;
    private ContatoSocial contatoSocial;

    public PessoaHasContatoSocial() {}

    public String getId() {
        return id;
    }

    public void setId(String idpessoa, String idcontatoSocial) {
        this.id = String.format(COMPOSITE_KEY, idpessoa, idcontatoSocial);
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public ContatoSocial getContatoSocial() {
        return contatoSocial;
    }

    public void setContatoSocial(ContatoSocial contatoSocial) {
        this.contatoSocial = contatoSocial;
    }
}
