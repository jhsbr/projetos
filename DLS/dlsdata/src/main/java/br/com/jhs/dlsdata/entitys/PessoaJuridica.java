package br.com.jhs.dlsdata.entitys;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by JHS
 * on 19/11/2015.
 */
public class PessoaJuridica extends RealmObject {

    @PrimaryKey
    private int idpessoaJuridica;
    private String cnpj;
    private String insc_estadual;
    private Pessoa pessoa;

    public PessoaJuridica() {}

    public int getIdpessoaJuridica() {
        return idpessoaJuridica;
    }

    public void setIdpessoaJuridica(int idpessoaJuridica) {
        this.idpessoaJuridica = idpessoaJuridica;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getInsc_estadual() {
        return insc_estadual;
    }

    public void setInsc_estadual(String insc_estadual) {
        this.insc_estadual = insc_estadual;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }
}
