package br.com.jhs.dlsdata.entitys;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by JHS
 * on 19/11/2015.
 */
public class ContatoSocial extends RealmObject {

    @PrimaryKey
    private int idcontatoSocial;
    private String descricao_rede_social;
    private String rede_social_user;
    private PessoaHasContatoSocial pessoaHasContatoSocial;

    public ContatoSocial() {}

    public int getIdcontatoSocial() {
        return idcontatoSocial;
    }

    public void setIdcontatoSocial(int idcontatoSocial) {
        this.idcontatoSocial = idcontatoSocial;
    }

    public String getDescricao_rede_social() {
        return descricao_rede_social;
    }

    public void setDescricao_rede_social(String descricao_rede_social) {
        this.descricao_rede_social = descricao_rede_social;
    }

    public String getRede_social_user() {
        return rede_social_user;
    }

    public void setRede_social_user(String rede_social_user) {
        this.rede_social_user = rede_social_user;
    }

    public PessoaHasContatoSocial getPessoaHasContatoSocial() {
        return pessoaHasContatoSocial;
    }

    public void setPessoaHasContatoSocial(PessoaHasContatoSocial pessoaHasContatoSocial) {
        this.pessoaHasContatoSocial = pessoaHasContatoSocial;
    }
}
