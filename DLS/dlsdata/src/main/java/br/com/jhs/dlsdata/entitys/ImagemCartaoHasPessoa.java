package br.com.jhs.dlsdata.entitys;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by JHS
 * on 19/11/2015.
 */
public class ImagemCartaoHasPessoa extends RealmObject {

    @Ignore
    private final String COMPOSITE_KEY = "%d;%d";

    @PrimaryKey
    private String id;
    private Pessoa pessoa;
    private ImagemCartao imagemCartao;

    public ImagemCartaoHasPessoa() {}

    public String getId() {
        return id;
    }

    public void setId(String idpessoa, String idimagem_cartao) {
        this.id = String.format(COMPOSITE_KEY, idpessoa, idimagem_cartao);
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public ImagemCartao getImagemCartao() {
        return imagemCartao;
    }

    public void setImagemCartao(ImagemCartao imagemCartao) {
        this.imagemCartao = imagemCartao;
    }
}
